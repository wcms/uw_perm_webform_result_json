<?php

/**
 * @file
 * uw_perm_webform_result_json.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_perm_webform_result_json_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer uw_webform_result_json'.
  $permissions['administer uw_webform_result_json'] = array(
    'name' => 'administer uw_webform_result_json',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_webform_result_json',
  );

  return $permissions;
}
